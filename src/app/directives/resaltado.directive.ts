import { Directive, ElementRef, HostListener,Input } from '@angular/core';

@Directive({
  selector: '[appResaltado]'
})
export class ResaltadoDirective {
  @Input('appResaltado') color: string = 'yellow';
  constructor(private el: ElementRef) {
    console.log('directiva soy');
  }

  @HostListener('mouseenter') mouseEntro() {

    this.el.nativeElement.style.backgroundColor = this.color;

  }
  @HostListener('mouseleave') mouseSalio() {

    this.el.nativeElement.style.backgroundColor = 'white';

  }
}
