import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-usuario-nuevo',
  template: `
    <p>
      usuario-nuevo works!
    </p>
  `,
  styles: []
})
export class UsuarioNuevoComponent implements OnInit {

  constructor(private rout:ActivatedRoute) {
    this.rout.parent.params.subscribe(parametros =>{
      console.log("ruta HIJA NUEVO");
      console.log(parametros.id);
    })
   }

  ngOnInit() {
  }

}
