import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html'
})
export class UsuarioComponent implements OnInit {

  constructor(private rout:ActivatedRoute) {
    this.rout.params.subscribe(parametros =>{
      console.log("ruta padre");
      console.log(parametros.id);
    })
   }

  ngOnInit() {
  }

}
