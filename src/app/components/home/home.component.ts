import { Component, OnInit,
  OnChanges,DoCheck,AfterContentInit,AfterContentChecked,
  AfterViewInit,AfterViewChecked,OnDestroy } from '@angular/core';

  @Component({
    selector: 'app-home',
    template: `
    <p [appResaltado]="'orange'">
    hola mundo
    </p>
    <app-ng-style></app-ng-style>
    <app-css class="m-auto"></app-css>

    <p>
    Hola mundo desde appComponent
    </p>
    <app-clases></app-clases>
    `,
    styles: []
  })
  export class HomeComponent implements OnInit,
  OnChanges,DoCheck,AfterContentInit,AfterContentChecked,
  AfterViewInit,AfterViewChecked,OnDestroy {

    constructor() { }

    ngOnInit(){
      console.log("ngOnInit");
    }
    ngOnChanges(){
      console.log("ngOnChanges");
    }
    ngDoCheck(){
      console.log("ngDoCheck");
    }
    ngAfterContentInit(){
      console.log("ngAfterContentInit");
    }
    ngAfterContentChecked(){
      console.log("ngAfterContentChecked");
    }
    ngAfterViewInit(){
      console.log("ngAfterViewInit");
    }
    ngAfterViewChecked(){
      console.log("ngAfterViewChecked");
    }
    ngOnDestroy(){
      console.log("ngOnDestroy");
    }
  }
