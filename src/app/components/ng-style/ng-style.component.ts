import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-style',
  template: `
    <p [style.fontSize.px] = "tamanio" >
      Hola mundo.... esta es una etiqueta
    </p>
    <input name="" id="" class="btn btn-primary" (click)="tamanio =tamanio + 1;" type="button" value="+">
    <input name="" id="" class="btn btn-danger" (click)="tamanio=tamanio -1;" type="button" value="-">
  `,
  styles: []
})
export class NgStyleComponent implements OnInit {
  tamanio:number = 40;
  constructor() { }

  ngOnInit() {
  }

}
